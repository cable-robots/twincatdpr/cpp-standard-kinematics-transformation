///////////////////////////////////////////////////////////////////////////////
// StandardKinematicsTransformationDriver.h

#ifndef __STANDARDKINEMATICSTRANSFORMATIONDRIVER_H__
#define __STANDARDKINEMATICSTRANSFORMATIONDRIVER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TcBase.h"

#define STANDARDKINEMATICSTRANSFORMATIONDRV_NAME        "STANDARDKINEMATICSTRANSFORMATION"
#define STANDARDKINEMATICSTRANSFORMATIONDRV_Major       1
#define STANDARDKINEMATICSTRANSFORMATIONDRV_Minor       0

#define DEVICE_CLASS CStandardKinematicsTransformationDriver

#include "ObjDriver.h"

class CStandardKinematicsTransformationDriver : public CObjDriver
{
public:
	virtual IOSTATUS	OnLoad();
	virtual VOID		OnUnLoad();

	//////////////////////////////////////////////////////
	// VxD-Services exported by this driver
	static unsigned long	_cdecl STANDARDKINEMATICSTRANSFORMATIONDRV_GetVersion();
	//////////////////////////////////////////////////////
	
};

Begin_VxD_Service_Table(STANDARDKINEMATICSTRANSFORMATIONDRV)
	VxD_Service( STANDARDKINEMATICSTRANSFORMATIONDRV_GetVersion )
End_VxD_Service_Table


#endif // ifndef __STANDARDKINEMATICSTRANSFORMATIONDRIVER_H__