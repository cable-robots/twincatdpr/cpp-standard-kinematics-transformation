// StandardKinematicsTransformationCtrl.cpp : Implementation of CTcStandardKinematicsTransformationCtrl
#include "TcPch.h"
#pragma hdrstop

#include "StandardKinematicsTransformationW32.h"
#include "StandardKinematicsTransformationCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CStandardKinematicsTransformationCtrl

CStandardKinematicsTransformationCtrl::CStandardKinematicsTransformationCtrl() 
	: ITcOCFCtrlImpl<CStandardKinematicsTransformationCtrl, CStandardKinematicsTransformationClassFactory>() 
{
}

CStandardKinematicsTransformationCtrl::~CStandardKinematicsTransformationCtrl()
{
}

