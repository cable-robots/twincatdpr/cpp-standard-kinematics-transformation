#pragma once

#include "TcPch.h"

#include "cminpack/cminpack.h"
#pragma hdrstop

/*
 * This file provides a few math related functions like `min_` and `max_`, but
 * also other things.
 */

#ifndef real
#define real __cminpack_real__
#endif // !real

#ifndef TWOPI
#define TWOPI 0.628318530717958647692528676655900576839433879875021164194988e1
#endif // !TWOPI


#ifndef NOMINMAX

#ifndef min_
#define min_(a,b) ((a) <= (b) ? (a) : (b))
#endif // !min_

#ifndef max_
#define max_(a,b) ((a) >= (b) ? (a) : (b))
#endif // !max_

#endif // !NOMINMAX

#ifndef abs_
#define abs_(x) ((x) >= 0 ? (x) : -(x))
#endif // !abs
