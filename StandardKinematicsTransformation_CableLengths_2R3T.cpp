#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 5
CableLengths_t cable_lengths(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  CableLengths_t lengths; // Calculated lengths

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t2;
  real t4;
  real t6;
  real t8;
  real t10;
  real t12;
  real t14;
  real t16;
  real t18;
  real t47;
  real t52;
  real t57;
  real t62;
  real t70;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t320 = pos[2];
  t400 = eul[0];
  t410 = eul[1];
  t4000 = sin_(t400);
  t4001 = cos_(t400);
  t4100 = sin_(t410);
  t4101 = cos_(t410);

  // Solve inverse kinematics loop for every cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t520 = aii[2];
    t600 = bii[0];
    t610 = bii[1];
    t620 = bii[2];

    // Intermediate values
    t2 = t600 * t600;
    t4 = t610 * t610;
    t6 = t320 * t320;
    t8 = t620 * t620;
    t10 = t300 * t300;
    t12 = t510 * t510;
    t14 = t310 * t310;
    t16 = t520 * t520;
    t18 = t500 * t500;
    t4001 = 0.2e1 * t4100 * t520 * t600 + 0.2e1 * t4101 * t600 * t300 - 0.2e1 * t4100 * t600 * t320 + 0.2e1 * t4000 * t510 * t620 - 0.2e1 * t4000 * t620 * t310 + t10 + t12 + t14 + t16 + t18 + t2 + t4 + t6 + t8;
    t47 = t4101 * t4001;
    t52 = t4101 * t4000;
    t57 = t4100 * t4001;
    t62 = t4100 * t4000;
    t70 = -t4101 * t500 * t600 - t4001 * t510 * t610 + t4001 * t610 * t310 - t52 * t520 * t610 - t47 * t520 * t620 - t62 * t500 * t610 - t57 * t500 * t620 + t52 * t610 * t320 + t62 * t610 * t300 + t47 * t620 * t320 + t57 * t620 * t300 - t510 * t310 - t520 * t320 - t500 * t300;

    // And solve for the cable length
    lengths[ic] = sqrt_(t4001 + 0.2e1 * t70);
  }

  return lengths;
}
#endif
