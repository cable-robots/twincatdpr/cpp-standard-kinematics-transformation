#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 2
CableLengths_t cable_lengths(const FrameAnchors_t &ai, PositionParametrization_t &pos) {
  // Counter variables
  unsigned int ic;

  // Return variables
  CableLengths_t lengths; // Calculated lengths

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t2;
  real t7;
  real t11;
  real t12;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];

  // Solve inverse kinematics loop for every cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    aii = ai[ic];
    // Another set of code generation assignments
    t500 = aii[0];
    t510 = aii[1];

    // Intermediate values
    t2 = t500 * t500;
    t7 = t510 * t510;
    t11 = t300 * t300;
    t12 = t310 * t310;

    // And solve for the cable length
    lengths[ic] = sqrt_(-0.2e1 * t500 * t300 - 0.2e1 * t510 * t310 + t11 + t12 + t2 + t7);
  }

  return lengths;
}
#endif
