#pragma once
#include <array>
#include "TcPch.h"
#include "Cnc/TcCncKinematicsInterfaces.h"
#include "RobotDocument.h"
#include "cminpack/cminpack.h"
#pragma hdrstop

/*
 * This file defines several types needed for proper type hinting and casting
 * of the backward and forward kinematics codes.
 */

#ifndef real
#define real __cminpack_real__
#endif // !real

typedef std::array<real, 1> Vector1_t;
typedef std::array<real, 2> Vector2_t;
typedef std::array<real, 3> Vector3_t;

typedef std::array<Vector2_t, 2> RotationMatrix2_t;
typedef std::array<Vector3_t, 3> RotationMatrix3_t;

typedef std::array<real, NUMBER_OF_CABLES> Residual_t;
typedef std::array<real, DOF_TOTAL> JacobianRow_t;
typedef std::array<JacobianRow_t, NUMBER_OF_CABLES> Jacobian_t;
typedef std::array<real, NUMBER_OF_CABLES * DOF_TOTAL> RowJacobian_t;

typedef struct {
  FrameAnchors_t ai; // frame anchors
  PlatformAnchors_t bi; // platform anchors
  CableLengths_t l0; // cable length at home pose
  CableLengths_t l; // active cable length i.e., from drives
} LevmarUserData_t;

real norm(Vector1_t &v);
real norm(Vector2_t &v);
real norm(Vector3_t &v);

RotationMatrix2_t rotation_matrix_2d(real &yaw);
RotationMatrix3_t rotation_matrix_3d(real &roll, real &pitch, real &yaw);

CableLengths_t cable_lengths(const FrameAnchors_t &ai, PositionParametrization_t &r);
CableLengths_t cable_lengths(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &r, OrientationParametrization_t &e);

Jacobian_t jacobian(const FrameAnchors_t &ai, PositionParametrization_t &r);
Jacobian_t jacobian(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &r, OrientationParametrization_t &e);

PositionParametrization_t guess_position(const FrameAnchors_t ai, CableLengths_t lengths);
PositionParametrization_t guess_position(const FrameAnchors_t ai, const PlatformAnchors_t bi, CableLengths_t lengths);

int levmar_callback(void *p, int m, int n, const real *x, real *fvec, real *fjac, int ldfjac, int iflag);

FrameAnchors_t parse_frame_anchors(PTcCncTrafoParameter p);
PlatformAnchors_t parse_platform_anchors(PTcCncTrafoParameter p);
