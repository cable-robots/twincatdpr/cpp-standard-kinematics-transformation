#pragma once

#include "TcPch.h"
#include "Mathematics.h"

#pragma hdrstop

/*
 * This file provides conversion methods for different often-used units like
 * radian to degree or meter to TwinCAT units.
 */

template<typename T>
constexpr auto RAD2DEG(T r) { return r * 180.0 / PI; }
template<typename T>
constexpr auto DEG2RAD(T d) { return d / 180.0 * PI; }

template<typename T>
constexpr auto METER2TCLINEAR(T m) { return m * 10000000.0; }
template<typename T>
constexpr auto TCLINEAR2METER(T l) { return l / 10000000.0; }

template<typename T>
constexpr auto RAD2TCANGULAR(T r) { return RAD2DEG(r) * 10000.0; }
template<typename T>
constexpr auto TCANGULAR2RAD(T d) { return DEG2RAD(d) / 10000.0; }
