#pragma once

#include "TcPch.h"


#pragma hdrstop

/*
 * This file defines the number of translational and rotational degrees of
 * freedom of the robot given its motion pattern.
 */

// Ensure "MOTION_PATTERN" has been defined
#ifndef  MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // ! MOTION_PATTERN


#if MOTION_PATTERN == 1
// 1T
constexpr auto DOF_TRANSLATION = 1;
constexpr auto DOF_ROTATION = 0;
#elif MOTION_PATTERN == 2
// 2T
constexpr auto DOF_TRANSLATION = 2;
constexpr auto DOF_ROTATION = 0;
#elif MOTION_PATTERN == 3
// 3T
constexpr auto DOF_TRANSLATION = 3;
constexpr auto DOF_ROTATION = 0;
#elif MOTION_PATTERN == 4
// 1R2T
constexpr auto DOF_TRANSLATION = 2;
constexpr auto DOF_ROTATION = 1;
#elif MOTION_PATTERN == 5
// 2R3T
constexpr auto DOF_TRANSLATION = 3;
constexpr auto DOF_ROTATION = 2;
#elif MOTION_PATTERN == 6
// 3R3T
constexpr auto DOF_TRANSLATION = 3;
constexpr auto DOF_ROTATION = 3;
#else
// Any other case
constexpr auto DOF_TRANSLATION = 0;
constexpr auto DOF_ROTATION = 0;
#endif

// also calculate the total number of degrees of freedom of the robot
constexpr auto DOF_TOTAL = DOF_TRANSLATION + DOF_ROTATION;

// row shift value for accessing consecutive elements of the same kinematic
// chain in `p->param`
constexpr auto PARAM_ROW_SHIFT = (DOF_ROTATION == 0) ? DOF_TRANSLATION : 2 * DOF_TRANSLATION;
