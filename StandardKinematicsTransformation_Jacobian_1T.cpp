#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Mathematics.h"
#include "Conversion.h"
#include "cminpack/cminpack.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 1
Jacobian_t jacobian(const FrameAnchors_t &ai, PositionParametrization_t &pos) {
  // Counter variables
  unsigned int ic;

  // Return variables
  Jacobian_t jac; // Jacobian at current pose

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor
  JacobianRow_t jaci;

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])
  
  real t1;

  // Code generation assignment
  t300 = pos[0];
  t1 = 0.2e1;
  
  // Loop over each cable and calculate its Jacobian
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    t500 = aii[0];

    // Cached expressions

    // Assign in derivative of the row
    jaci[0] = t1 * (-t500 + t300);

    // Assign in global Jacobian matrix
    jac[ic] = jaci;
  }

  return jac;
}
#endif
