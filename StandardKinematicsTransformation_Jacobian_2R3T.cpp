#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Mathematics.h"
#include "Conversion.h"
#include "cminpack/cminpack.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 5
Jacobian_t jacobian(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  Jacobian_t jac; // Jacobian at current pose

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor
  JacobianRow_t jaci;

  // Code-generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t5;
  real t6;
  real t7;
  real t8;
  real t9;
  real t10;
  real t11;
  real t12;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t320 = pos[2];
  t400 = eul[0];
  t410 = eul[1];
  t4001 = cos_(t400);
  t4100 = sin_(t410);
  t4101 = cos_(t410);
  t4000 = sin_(t400);
  t12 = 0.2e1;

  // Loop over each cable and calculate its Jacobian
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t520 = aii[2];
    t600 = aii[0];
    t610 = aii[1];
    t620 = aii[2];

    // Cached expressions
    t5 = t610 * t4000;
    t6 = t4001 * t620 + t5;
    t7 = t320 - t520;
    t8 = -t500 + t300;
    t9 = t310 - t510;
    t10 = t4100 * t8;
    t11 = t4101 * t7;

    // Assign in derivative of the row
    jaci[0] = t12 * (t4100 * t6 + t4101 * t600 - t500 + t300);
    jaci[1] = t12 * (t4001 * t610 - t4000 * t620 - t510 + t310);
    jaci[2] = t12 * (-t4100 * t600 + t4101 * t6 - t520 + t320);
    jaci[3] = t12 * (t4001 * (t10 * t610 + t11 * t610 - t9 * t620) - t4000 * (t9 * t610 + (t11 + t10) * t620));
    jaci[4] = t12 * (-t4001 * t620 * (t4100 * t7 - t4101 * t8) - t4100 * (t5 * t7 + t8 * t600) + t4101 * (t5 * t8 - t7 * t600));

    // Assign in global Jacobian matrix
    jac[ic] = jaci;
  }

  return jac;
}
#endif
