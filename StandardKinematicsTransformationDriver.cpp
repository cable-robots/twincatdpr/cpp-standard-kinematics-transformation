///////////////////////////////////////////////////////////////////////////////
// StandardKinematicsTransformationDriver.cpp
#include "TcPch.h"
#pragma hdrstop

#include "StandardKinematicsTransformationDriver.h"
#include "StandardKinematicsTransformationClassFactory.h"

DECLARE_GENERIC_DEVICE(STANDARDKINEMATICSTRANSFORMATIONDRV)

IOSTATUS CStandardKinematicsTransformationDriver::OnLoad( )
{
	TRACE(_T("CObjClassFactory::OnLoad()\n") );
	m_pObjClassFactory = new CStandardKinematicsTransformationClassFactory();

	return IOSTATUS_SUCCESS;
}

VOID CStandardKinematicsTransformationDriver::OnUnLoad( )
{
	delete m_pObjClassFactory;
}

unsigned long _cdecl CStandardKinematicsTransformationDriver::STANDARDKINEMATICSTRANSFORMATIONDRV_GetVersion( )
{
	return( (STANDARDKINEMATICSTRANSFORMATIONDRV_Major << 8) | STANDARDKINEMATICSTRANSFORMATIONDRV_Minor );
}

