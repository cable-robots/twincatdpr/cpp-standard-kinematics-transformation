#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Mathematics.h"
#include "Conversion.h"
#include "cminpack/cminpack.h"
#pragma hdrstop

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 6
Jacobian_t jacobian(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  Jacobian_t jac; // Jacobian at current pose

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor
  JacobianRow_t jaci;

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t4;
  real t5;
  real t9;
  real t10;
  real t11;
  real t12;
  real t13;
  real t14;
  real t15;
  real t16;
  real t17;
  real t18;
  real t19;
  real t20;
  real t21;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t320 = pos[2];
  t400 = eul[0];
  t410 = eul[1];
  t420 = eul[2];
  t4000 = sin_(t400);
  t4001 = cos_(t400);
  t4100 = sin_(t410);
  t4101 = cos_(t410);
  t4200 = sin_(t420);
  t4201 = cos_(t420);
  t19 = 0.2e1;

  // Loop over each cable and calculate its Jacobian
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t520 = aii[2];
    t600 = bii[0];
    t610 = bii[1];
    t620 = bii[2];

    // Cached expressions
    t4 = t620 * t4201;
    t5 = t610 * t4200;
    t9 = t610 * t4000;
    t10 = t4101 * t600;
    t11 = t4100 * t600;
    t12 = -t500 + t300;
    t13 = t310 - t510;
    t14 = t620 * t12;
    t15 = t610 * t13;
    t16 = t14 * t4100 + t15;
    t17 = t320 - t520;
    t18 = t620 * t13;
    t19 = t610 * t12;
    t20 = -t18 * t4100 + t19;
    t21 = t4101 * t17;
    t18 = t19 * t4100 - t18;
    t14 = t15 * t4100 + t14;
    t15 = t4200 * t13;
    t17 = t4100 * t17;

    // Assign in derivative of the row
    jaci[0] = t19 * (t4201 * (t9 * t4100 + t10) + (t4 * t4100 - t5) * t4001 - t500 + t300 + t4200 * t4000 * t620);
    jaci[1] = t19 * (t4001 * (t4200 * t620 * t4100 + t4201 * t610) + t4000 * (t5 * t4100 - t4) - t510 + t310 + t10 * t4200);
    jaci[2] = t19 * (t4101 * (t4001 * t620 + t9) - t520 + t320 - t11);
    jaci[3] = t19 * (t4001 * (t14 * t4200 + t18 * t4201 + t21 * t610) + t4000 * (-t16 * t4201 + t20 * t4200 - t21 * t620));
    jaci[4] = -t19 * (t4201 * t12 * (-t9 * t4101 + t11) + t4001 * t620 * (-t4101 * (t12 * t4201 + t15) + t17) + t4000 * t610 * (-t15 * t4101 + t17) + (t15 * t4100 + t21) * t600);
    jaci[5] = -t19 * (t4200 * (t10 * t12 + t18 * t4000) + t4001 * (t16 * t4200 + t4201 * t20) - t4000 * t4201 * t14 - t10 * t13 * t4201);

    // Assign in global Jacobian matrix
    jac[ic] = jaci;
  }

  return jac;
}
#endif
