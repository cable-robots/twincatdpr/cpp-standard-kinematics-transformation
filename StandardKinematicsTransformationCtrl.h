///////////////////////////////////////////////////////////////////////////////
// StandardKinematicsTransformationCtrl.h

#ifndef __STANDARDKINEMATICSTRANSFORMATIONCTRL_H__
#define __STANDARDKINEMATICSTRANSFORMATIONCTRL_H__

#include <atlbase.h>
#include <atlcom.h>


#include "resource.h"       // main symbols
#include "StandardKinematicsTransformationW32.h"
#include "TcBase.h"
#include "StandardKinematicsTransformationClassFactory.h"
#include "TcOCFCtrlImpl.h"

class CStandardKinematicsTransformationCtrl 
	: public CComObjectRootEx<CComMultiThreadModel>
	, public CComCoClass<CStandardKinematicsTransformationCtrl, &CLSID_StandardKinematicsTransformationCtrl>
	, public IStandardKinematicsTransformationCtrl
	, public ITcOCFCtrlImpl<CStandardKinematicsTransformationCtrl, CStandardKinematicsTransformationClassFactory>
{
public:
	CStandardKinematicsTransformationCtrl();
	virtual ~CStandardKinematicsTransformationCtrl();

DECLARE_REGISTRY_RESOURCEID(IDR_STANDARDKINEMATICSTRANSFORMATIONCTRL)
DECLARE_NOT_AGGREGATABLE(CStandardKinematicsTransformationCtrl)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CStandardKinematicsTransformationCtrl)
	COM_INTERFACE_ENTRY(IStandardKinematicsTransformationCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl2)
END_COM_MAP()

};

#endif // #ifndef __STANDARDKINEMATICSTRANSFORMATIONCTRL_H__
