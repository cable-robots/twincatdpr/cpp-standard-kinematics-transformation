#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Conversion.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 6
HRESULT CStandardKinematicsTransformation::Backward(PTcCncTrafoParameter p)
{
  // Counter variables
  unsigned int ic; //cable counter
  unsigned int ilin; // linear dof counter
  unsigned int irot; // rotation dof counter

  // Positions
  PositionParametrization_t pos; // commanded position
  PositionParametrization_t pos_home; // home position

  // Orientations
  OrientationParametrization_t eul; // commanded orientation in Tait-Bryan convention
  OrientationParametrization_t eul_home; // home orientation in Tait-Bryan convention

  // Geometry
  FrameAnchors_t frame_anchors; // array[M] of FrameAnchor_t
  PlatformAnchors_t platform_anchors; // arrray[M] of PlatformAnchor_t
  FrameAnchor_t ai; // single frame anchor, array of [3] real
  PlatformAnchor_t bi; // single platform anchor, array[3] of real

  // Calculated cable lengths
  CableLengths_t lengths; // array[M] of CableLength_t
  CableLengths_t lengths_home; // array[M] of CableLength_t

  // Push commanded and home position
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    pos_home[ilin] = 0.0;
    pos[ilin] = p->i[ilin + 0];
  }
  // Push commanded and home orientation
  for (irot = 0; irot < DOF_ROTATION; irot++) {
    eul_home[irot] = 0.0;
    eul[irot] = TCANGULAR2RAD(p->i[irot + DOF_TRANSLATION]);
  }

  // Parse robot geometry
  frame_anchors = parse_frame_anchors(p);
  platform_anchors = parse_platform_anchors(p);

  // Calculate commanded and home length
  lengths = cable_lengths(frame_anchors, platform_anchors, pos, eul);
  lengths_home = cable_lengths(frame_anchors, platform_anchors, pos_home, eul_home);

  // And loop over every cable to determine the relative cable length for the given pose
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    p->o[ic] = lengths[ic] - lengths_home[ic];
  }

  return S_OK;
}
#endif
