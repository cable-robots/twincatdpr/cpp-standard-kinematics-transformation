#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 6
CableLengths_t cable_lengths(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  CableLengths_t lengths; // Calculated lengths

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor

  // Code generation variables
  real t300; // pos[0]
  real t310; // pos[1]
  real t320; // pos[2]
  real t400; // eul[0]
  real t410; // eul[1]
  real t420; // eul[2]
  real t500; // aii[0]
  real t510; // aii[1]
  real t520; // aii[2]
  real t600; // bii[0]
  real t610; // bii[1]
  real t620; // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t2;
  real t4;
  real t6;
  real t8;
  real t10;
  real t12;
  real t14;
  real t16;
  real t18;
  real t36;
  real t44;
  real t53;
  real t57;
  real t61;
  real t66;
  real t71;
  real t76;
  real t81;
  real t86;
  real t90;
  real t96;
  real t99;
  real t110;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t320 = pos[2];
  t400 = eul[0];
  t410 = eul[1];
  t420 = eul[2];
  t4100 = sin_(t410);
  t4101 = cos_(t410);
  t4001 = cos_(t400);
  t4000 = sin_(t400);
  t4201 = cos_(t420);
  t4200 = sin_(t420);
  t36 = t4101 * t4001;
  t44 = t4101 * t4000;
  t53 = t4101 * t4201;

  // Solve inverse kinematics loop for every cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t520 = aii[2];
    t600 = bii[0];
    t610 = bii[1];
    t620 = bii[2];

    // Intermediate values
    t2 = t500 * t500;
    t4 = t300 * t300;
    t6 = t510 * t510;
    t8 = t310 * t310;
    t10 = t520 * t520;
    t12 = t320 * t320;
    t14 = t620 * t620;
    t16 = t600 * t600;
    t18 = t610 * t610;
    t57 = -0.2e1* t53 * t500 * t600 + 0.2e1* t36 * t620 * t320 - 0.2e1* t4100 * t600 * t320 + 0.2e1* t44 * t610 * t320 - 0.2e1* t36 * t520 * t620 + 0.2e1* t4100 * t520 * t600 - 0.2e1* t44 * t520 * t610 - 0.2e1* t500 * t300 - 0.2e1* t520 * t320 - 0.2e1* t510 * t310 + t10 + t12 + t14 + t16 + t18 + t2 + t4 + t6 + t8;
    t61 = t4101 * t4200;
    t66 = t4201 * t4001;
    t71 = t4201 * t4000;
    t76 = t4200 * t4001;
    t81 = t4200 * t4000;
    t86 = t4100 * t4201;
    t90 = t4001 * t620;
    t96 = t4000 * t610;
    t99 = t4100 * t4200;
    t110 = -t86 * t4001 * t500 * t620 - t86 * t4000 * t500 * t610 - t99 * t4001 * t510 * t620 - t99 * t4000 * t510 * t610 - t81 * t500 * t620 + t76 * t500 * t610 + t81 * t620 * t300 + t71 * t510 * t620 - t71 * t620 * t310 + t53 * t600 * t300 - t61 * t510 * t600 + t61 * t600 * t310 - t76 * t610 * t300 - t66 * t510 * t610 + t66 * t610 * t310 + t86 * t90 * t300 + t86 * t96 * t300 + t99 * t90 * t310 + t99 * t96 * t310;

    // And solve for the cable length
    lengths[ic] = sqrt_(t57 + 0.2e1* t110);
  }

  return lengths;
}
#endif
