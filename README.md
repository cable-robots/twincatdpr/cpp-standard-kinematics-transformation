# TwinCATdpr - Standard Kinematics Transformation C++ Driver

This project is part of TwinCATdpr and provides the C++ standard kinematics transformation code.

## Disclaimer

Make sure to read the official TwinCAT 3 Manual on Source Control before proceeding: https://download.beckhoff.com/download/document/automation/twincat3/TC3_SourceControl_EN.pdf

## Authors

* Philipp Tempel <p.tempel@tudelft.nl> https://philipptempel.me
* Thomas Reichenbach <thomas.reichenbach@isw.uni-stuttgart.de>
* Felix Trautwein <felix.trautwein@isw.uni-stuttgart.de>