#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Mathematics.h"
#include "Conversion.h"
#include "cminpack/cminpack.h"
#pragma hdrstop

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 2
HRESULT CStandardKinematicsTransformation::Forward(PTcCncTrafoParameter p)
{
  // Counter variables
  unsigned int ic; //cable counter
  unsigned int ilin; // linear dof counter

  // Positions
  PositionParametrization_t pos; // estimated position
  PositionParametrization_t pos_home; // home position
  PositionParametrization_t position_guess; // 

  // Poses
  PoseParametrization_t pose; // initial and later on estimated pose value

  // Geometry
  FrameAnchors_t frame_anchors; // array[M] of FrameAnchor_t
  FrameAnchor_t ai; // single frame anchor, array of [3] real

  // Calculated cable lengths
  CableLengths_t lengths; // array[M] of CableLength_t
  CableLengths_t lengths_home; // array[M] of CableLength_t

  // Arguments used in levmar callback
  Residual_t residual;
  RowJacobian_t jac;
  LevmarUserData_t data;

  // Variables used in levmar optimizer

  /*
   * set ftol and xtol to the square root of the machine and gtol to zero.
   * Unless high solutions are required, these are the recommended settings.
   */
   //real ftol; // function tolerance
   //real xtol; // state tolerance
   //real gtol; // constraint tolerance
  real tol; // tolerance used for both ftol and xtol

  /*
   * info is an integer output variable. if the user has terminated execution,
   * info is set to the (negative) value of iflag. see description of fcn.
   * otherwise, info is set as given in `lmder1.c`.
   */
  int info; // return value of levmar optimizer; 0 == success, else failure


  // lwa is a positive integer input variable not less than 5 * n + m.
  const int lwa = 6 * DOF_TOTAL + NUMBER_OF_CABLES; // length of levmar's work array `wa`

  // wa is a work array of length lwa.
  std::array<real, lwa> wa; // levmar's work array

  /*
   * ldfjac is a positive integer input variable not less than m which
   * specifies the leading dimension of the array fjac.
   */
  int ldfjac = NUMBER_OF_CABLES;

  /*
   * `ipvt` is an integer output array of length n. `ipvt` defines a
   * permutation matrix `p` such that `jac*p = q*r`, where `jac` is the final
   * calculated jacobian, `q` is orthogonal (not stored), and `r` is upper
   * triangular with diagonal elements of nonincreasing magnitude. Column `j`
   * of `p` is column `ipvt(j)` of the identity matrix.
   */
  std::array<int, DOF_TOTAL> ipvt; // permutation matrix p `jac*p = q*r`

  // Populate home position and orientation
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    pos_home[ilin] = 0.0;
  }

  // Parse robot geometry
  frame_anchors = parse_frame_anchors(p);

  // Determine cable length at home pose
  lengths_home = cable_lengths(frame_anchors, pos_home);
  // Actual cable length is the set value (relative to the home position) plus
  // the cable length at home position
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    lengths[ic] = p->i[ic] + lengths_home[ic];
  }

  // Calculate initial position guess
  position_guess = guess_position(frame_anchors, lengths);
  // Populate initial pose guess from ...
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    // ... translation
    pose[ilin + 0] = position_guess[ilin + 0];
  }

  // Populate configuration of levmar
  data.ai = frame_anchors;
  data.l0 = lengths_home;
  data.l = lengths;

  /*
   * set ftol and xtol to the square root of the machine and gtol to zero.
   * Unless high solutions are required, these are the recommended settings.
   */
   //ftol = sqrt_(__cminpack_func__(dpmpar)(1));
   //xtol = sqrt_(__cminpack_func__(dpmpar)(1));
   //gtol = 0.0;
  tol = sqrt_(__cminpack_func__(dpmpar)(1));

  //// Call LEVMAR and solve the minimization problem
  //// This converts new C++11-styled std::array to pre-C++11-styled arrays of doubles
  //// @see https://stackoverflow.com/a/2923290/4065558
  info = __cminpack_func__(lmder1)(levmar_callback, // levmar callback (lmder1:p)
    &data, // additional user data 
    NUMBER_OF_CABLES, // (lmder1:m)
    DOF_TOTAL, // (lmder1:n)
    &pose[0], // pose estiamte (lmder1:x)
    &residual[0], // vector residual (lmder1:fvec)
    &jac[0], // function Jacobian (lmder1:fjac)
    ldfjac, // leading dimension of Jacobian (lmder1:ldfjac)
    tol, // tolerance (function and step) (lmder1:tol)
    &ipvt[0], // permutation matrix `jac*p = q*r`
    &wa[0], // work array
    lwa // length of work array
    );

  // Push estimated linear position into output
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    p->o[ilin] = pose[ilin];
  }

  /*
   * Only return S_OK if levmar's returned `info` is anything between
   *  1 ... algorithm estimates that the relative error in the sum of squares
   *        is at most `tol`.
   *  2 ... algorithm estimates that the relative error between `x` and the
   *        solution is at most `tol`
   *  3 ... conditions for info = 1 and info = 2 both hold.
   * else return S_FALSE
   */
  return IFTE(1 <= info && info <= 4, S_OK, S_FALSE);
}

int levmar_callback(void *p,
  int m, // number of functions
  int n, // number of free variables
  const real *x, // current estimate
  real *fvec, // residual vector
  real *fjac, // Jacobian of residual vector
  int ldfjac, // 
  int iflag) {
  // Counter variables
  unsigned int ic; //cable counter
  unsigned int ilin; // linear dof counter
  unsigned int irot; // rotation dof counter
  unsigned int idof; // degree of freedom counter

  // Robot configuration and reference values; constant
  // const CableLengths_t length_home = ((LevmarUserData_t*)p)->l0; // array[M] of cable lengths at home position
  const FrameAnchors_t frame_anchors = ((LevmarUserData_t*)p)->ai; // array[M] of FrameAnchor_t
  const CableLengths_t lengths = ((LevmarUserData_t*)p)->l; // array[M] of current cable lengths i.e., drive positions

  // Position
  PositionParametrization_t pos; // array[N_LIN] of position

  // Values calculated in the callback
  CableLengths_t lengths_estimate; // array[M] of lengths at current estimate X
  Jacobian_t jac; // array[M] of JacobianRow_t (which is array[N_tot] of real)

  // Local variable
  CableLength_t le; // length estimate
  CableLength_t l; // actual length from drive

  // Parse current estimate of the pose as ...
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    // ... position
    pos[ilin] = x[ilin];
  }

  // Calculate the function at X and return in FVEC
  if (iflag != 2) {
    // Calculate cable length for the estimate
    lengths_estimate = cable_lengths(frame_anchors, pos);

    // Calculate residual of every vector
    for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
      // Quicker access to the array entries
      le = lengths_estimate[ic];
      l = lengths[ic];

      // Residual as l(x)^2 - l^2
      fvec[ic] = sqr_(le) - sqr_(l);
    }
  }
  // Calculate Jacobian at X and return matrix in FJAC
  else {
    // Determine jacobian
    jac = jacobian(frame_anchors, pos);

    // Shift register from result into return variable
    for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
      // Loop over each derivative dimension...
      for (idof = 0; idof < DOF_TOTAL; idof++) {
        // ...and shift register
        fjac[ic + ldfjac * idof] = jac[ic][idof];
      }
    }
  }

  // No need to flag a stop from here i.e., return `success`
  return 0;
}
#endif
