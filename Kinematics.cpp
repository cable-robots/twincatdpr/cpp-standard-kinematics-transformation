#include "Kinematics.h"

real norm(Vector1_t &v) {
  return fabs_(v[0]);
}

real norm(Vector2_t &v) {
  return sqrt_(sqr_(v[0]) + sqr_(v[1]));
}

real norm(Vector3_t &v) {
  return sqrt_(sqr_(v[0]) + sqr_(v[1]) + sqr_(v[2]));
}

RotationMatrix2_t rotation_matrix_2d(real &yaw) {
  RotationMatrix2_t R;

  real t1;
  real t2;

  t1 = cos_(yaw);
  t2 = sin_(yaw);

  R[0][0] = t1;
  R[0][1] = -t2;
  R[1][0] = t2;
  R[1][1] = t1;

  return R;
}

RotationMatrix3_t rotation_matrix_3d(real &roll, real &pitch, real &yaw) {
  RotationMatrix3_t R;

  real t1;
  real t2;
  real t4;
  real t5;
  real t6;
  real t8;
  real t9;
  real t12;

  t1 = cos_(pitch);
  t2 = cos_(yaw);
  t4 = sin_(roll);
  t5 = sin_(pitch);
  t6 = t4 * t5;
  t8 = cos_(roll);
  t9 = sin_(yaw);
  t12 = t8 * t5;
  R[0][0] = t1 * t2;
  R[0][1] = t6 * t2 - t8 * t9;
  R[0][2] = t12 * t2 + t4 * t9;
  R[1][0] = t1 * t9;
  R[1][1] = t8 * t2 + t6 * t9;
  R[1][2] = t12 * t9 - t4 * t2;
  R[2][0] = -t5;
  R[2][1] = t4 * t1;
  R[2][2] = t8 * t1;

  return R;
}

FrameAnchors_t parse_frame_anchors(PTcCncTrafoParameter p) {
  unsigned int ic;
  unsigned int ilin;
  FrameAnchors_t ai;
  FrameAnchor_t aii;

  // Loop over each cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Loop over each linear dimension
    for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
      // And read the shifted ai value
      aii[ilin] = p->para[ilin + ic * PARAM_ROW_SHIFT + 0];
    }
    ai[ic] = aii;
  }

  return ai;
}

PlatformAnchors_t parse_platform_anchors(PTcCncTrafoParameter p) {
  unsigned int ic;
  unsigned int ilin;
  PlatformAnchors_t bi;
  PlatformAnchor_t bii;

  // Loop over each cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Loop over each linear dimension
    for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
      // And read the shifted bi value
      bii[ilin] = p->para[ilin + ic * PARAM_ROW_SHIFT + DOF_TRANSLATION];
    }
    bi[ic] = bii;
  }

  return bi;
}

PositionParametrization_t guess_position(const FrameAnchors_t ai, CableLengths_t lengths) {
  // Loop variables
  unsigned int ic; // cable counter
  unsigned int ilin; // linear dof counter
  Vector3_t lowers; // array[3] of lower bound of initial position guess
  Vector3_t uppers; // array[3] of upper bound of initial position guess

  // Inside-loop variables
  FrameAnchor_t aii; // array[3] of single frame anchor position
  CableLength_t length; // cable length accounted for platform anchor distance
  real lower; // lower value of the current cable's axis estimate
  real upper; // upper value of the current cable's axis estimate

  // Return variable
  PositionParametrization_t guess; // array[3] of guess of initial position

  // Initialize lower and upper boundaries
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    lowers[ilin] = INFINITY;
    uppers[ilin] = -INFINITY;
  }

  // Loop over each cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Quicker array access
    aii = ai[ic];

    // Loop over each linear dimension
    for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
      // Extract cable length from array for quicker access
      length = lengths[ic];
      // Determine upper and lower bound around the anchor's current coordinate
      lower = aii[ilin] - length;
      upper = aii[ilin] + length;

      // Update the global lower and upper boundary
      lowers[ilin] = min_(lowers[ilin], lower);
      uppers[ilin] = max_(uppers[ilin], upper);
    }
  }

  // Calculate mean value of upper and lower boundaries
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    guess[ilin] = (lowers[ilin] + uppers[ilin]) / 2.0;
  }

  // Return guess
  return guess;
}


PositionParametrization_t guess_position(const FrameAnchors_t ai, const PlatformAnchors_t bi, CableLengths_t lengths) {
  // Loop variables
  unsigned int ic; // cable counter
  unsigned int ilin; // linear dof counter
  Vector3_t lowers; // array[3] of lower bound of initial position guess
  Vector3_t uppers; // array[3] of upper bound of initial position guess

  // Inside-loop variables
  FrameAnchor_t aii; // array[3] of single frame anchor position
  PlatformAnchor_t bii; // array[3] of single platform anchor position
  CableLength_t length; // cable length accounted for platform anchor distance
  real lower; // lower value of the current cable's axis estimate
  real upper; // upper value of the current cable's axis estimate

  // Return variable
  PositionParametrization_t guess; // array[3] of guess of initial position

  // User-decided switch if the bounding box pose estimate should be used or
  // not
#if defined BOUNDINGBOX_POSEESTIMATE && BOUNDINGBOX_POSEESTIMATE
  // Initialize lower and upper boundaries
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    lowers[ilin] = INFINITY;
    uppers[ilin] = -INFINITY;
  }

  // Loop over each cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Quicker array access
    aii = ai[ic];
    bii = bi[ic];

    // Loop over each linear dimension
    for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
      // Offset cable length by the length of the distance of the platform
      // anchor
      length = lengths[ic] + norm(bii);
      // Determine upper and lower bound around the anchor's current coordinate
      lower = aii[ilin] - length;
      upper = aii[ilin] + length;

      // Update the global lower and upper boundary
      lowers[ilin] = min_(lowers[ilin], lower);
      uppers[ilin] = max_(uppers[ilin], upper);
    }
  }

  // Calculate mean value of upper and lower boundaries
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    guess[ilin] = (lowers[ilin] + uppers[ilin]) / 2.0;
  }
#else
  // Set zero-pose as pose guess
  for (ilin = 0; ilin < DOF_TRANSLATION; ilin++) {
    guess[ilin] = 0.0;
  }
#endif

  // Return guess
  return guess;
}
