#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 4
CableLengths_t cable_lengths(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  CableLengths_t lengths; // Calculated lengths

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t2;
  real t4;
  real t7;
  real t9;
  real t11;
  real t13;
  real t15;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t400 = eul[0];
  t4001 = cos_(t400);
  t4000 = sin_(t400);

  // Solve inverse kinematics loop for every cable
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t600 = bii[0];
    t610 = bii[1];

    // Intermediate values
    t2 = t600 * t600;
    t4 = t610 * t610;
    t7 = t500 * t500;
    t9 = t510 * t510;
    t11 = t300 * t300;
    t13 = t310 * t310;
    t15 = -0.2e1 * t4000 * t510 * t600 + 0.2e1 * t4001 * t600 * t300 + 0.2e1 * t4000 * t600 * t310 - 0.2e1 * t4001 * t500 * t600 - 0.2e1 * t4001 * t510 * t610 - 0.2e1 * t4000 * t610 * t300 + 0.2e1 * t4001 * t610 * t310 + 0.2e1 * t4000 * t500 * t610 - 0.2e1 * t510 * t310 - 0.2e1 * t500 * t300 + t2 + t7 + t9 + t4 + t11 + t13;

    // And solve for the cable length
    lengths[ic] = sqrt_(t15);
  }

  return lengths;
}
#endif
