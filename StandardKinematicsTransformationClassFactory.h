///////////////////////////////////////////////////////////////////////////////
// StandardKinematicsTransformation.h

#pragma once

#include "ObjClassFactory.h"

class CStandardKinematicsTransformationClassFactory : public CObjClassFactory
{
public:
	CStandardKinematicsTransformationClassFactory();
	DECLARE_CLASS_MAP()
};


