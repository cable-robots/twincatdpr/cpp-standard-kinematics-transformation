#include "StandardKinematicsTransformation.h"
#include "RobotDocument.h"
#include "Kinematics.h"
#include "Mathematics.h"
#include "Conversion.h"
#include "cminpack/cminpack.h"

#ifndef MOTION_PATTERN
#define MOTION_PATTERN 0
#endif // !MOTION_PATTERN

#if defined MOTION_PATTERN && MOTION_PATTERN == 4
Jacobian_t jacobian(const FrameAnchors_t &ai, const PlatformAnchors_t &bi, PositionParametrization_t &pos, OrientationParametrization_t &eul) {
  // Counter variables
  unsigned int ic;

  // Return variables
  Jacobian_t jac; // Jacobian at current pose

  // Local variables inside the for-loop
  FrameAnchor_t aii; // single frame anchor
  PlatformAnchor_t bii; // single platform anchor
  JacobianRow_t jaci;

  // Code generation variables
  real t300;  // pos[0]
  real t310;  // pos[1]
  real t320;  // pos[2]
  real t400;  // eul[0]
  real t410;  // eul[1]
  real t420;  // eul[2]
  real t500;  // aii[0]
  real t510;  // aii[1]
  real t520;  // aii[2]
  real t600;  // bii[0]
  real t610;  // bii[1]
  real t620;  // bii[2]
  real t4000; // sin_(eul[0])
  real t4001; // cos_(eul[0])
  real t4100; // sin_(eul[1])
  real t4101; // cos_(eul[1])
  real t4200; // sin_(eul[2])
  real t4201; // cos_(eul[2])

  real t3;
  real t4;
  real t5;

  // Code generation assignment
  t300 = pos[0];
  t310 = pos[1];
  t400 = eul[0];
  t4001 = cos_(t400);
  t4000 = sin_(t400);
  t5 = 0.2e1;

  // Loop over each cable and calculate its Jacobian
  for (ic = 0; ic < NUMBER_OF_CABLES; ic++) {
    // Another set of code generation assignments
    aii = ai[ic];
    bii = bi[ic];
    t500 = aii[0];
    t510 = aii[1];
    t600 = bii[0];
    t610 = bii[1];

    // Cached expressions
    t3 = -t500 + t300;
    t4 = t310 - t510;
    
    // Assign in derivative of the row
    jaci[0] = t5 * (t4001 * t600 - t4000 * t610 - t500 + t300);
    jaci[1] = t5 * (t4001 * t610 + t4000 * t600 - t510 + t310);
    jaci[2] = -t5 * (-t4001 * (-t3 * t610 + t4 * t600) + t4000 * (t3 * t600 + t4 * t610));

    // Assign in global Jacobian matrix
    jac[ic] = jaci;
  }

  return jac;
}
#endif
