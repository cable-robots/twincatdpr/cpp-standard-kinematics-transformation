#pragma once
#include <array>
#include "TcPch.h"


#pragma hdrstop

/*
 * This file defines the overall robot structure i.e., its number of cables and
 * its motion pattern
 */

// Set your robot's number of cables here
#ifndef NUBMER_OF_CABLES
#define NUMBER_OF_CABLES 4
#endif // !NUBMER_OF_CABLES

/*
 * Choose your robot's motion pattern according to the following enumerate list
 * 
 * 1T     1
 * 2T     2
 * 3T     3
 * 1R2T   4
 * 2R3T   5
 * 3R3T   6
 */
#ifndef MOTION_PATTERN
#define MOTION_PATTERN 4
#endif // !MOTION_PATTERN

// Choose whether the bounding box pose estimate should be used for the forward
// kinematics
#ifndef BOUNDINGBOX_POSEESTIMATE
#define BOUNDINGBOX_POSEESTIMATE false
#endif // !BOUNDINGBOX_POSEESTIMATE


/*
 * 
 * LEAVE CODE UNTOUCHED FROM HERE ON DOWN
 * 
 */

/*
 * IFTE: IFThisElse, a short-hand wrapper for ternary operator in C++
 * implemented as compiler directive
 */
#ifndef IFTE
#define IFTE(cond, a, b) ((cond) ? (a) : (b));
#endif // !IFTE

// Include definition of motion patterns
#include "MotionPattern.h"

// Include mathematics header defining data types
#include "Mathematics.h"


typedef std::array<real, DOF_TRANSLATION> FrameAnchor_t;
typedef std::array<FrameAnchor_t, NUMBER_OF_CABLES> FrameAnchors_t;

typedef std::array<real, DOF_TRANSLATION> PlatformAnchor_t;
typedef std::array<PlatformAnchor_t, NUMBER_OF_CABLES> PlatformAnchors_t;

typedef real CableLength_t;
typedef std::array<CableLength_t, NUMBER_OF_CABLES> CableLengths_t;

typedef std::array<real, DOF_TRANSLATION> PositionParametrization_t;
typedef std::array<real, DOF_ROTATION> OrientationParametrization_t;

typedef std::array<real, DOF_TOTAL> PoseParametrization_t;
